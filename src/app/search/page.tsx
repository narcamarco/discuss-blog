import { redirect } from "next/navigation";
import React from "react";
import { fetchPostsBySearchTerm } from "@/db/queries/posts";
import PostList from "@/components/posts/post-list";

interface SearchPageprops {
  searchParams: {
    term: string;
  };
}

const SearchPage = async ({ searchParams }: SearchPageprops) => {
  const { term } = searchParams;

  if (!term) {
    redirect("/");
  }

  return (
    <div>
      <PostList fetchData={() => fetchPostsBySearchTerm(term)} />
    </div>
  );
};

export default SearchPage;
